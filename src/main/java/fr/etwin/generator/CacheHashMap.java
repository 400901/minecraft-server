package fr.etwin.generator;

import java.util.LinkedHashMap;
import java.util.Map;

public class CacheHashMap<K,V> extends LinkedHashMap<K,V>{
    private final int maxCapacity;

    public CacheHashMap(int maxCapacity) {
        super(maxCapacity);
        this.maxCapacity = maxCapacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() >= maxCapacity;
    }
}
