package fr.etwin.generator.providers;

import fr.etwin.generator.KubeChunkProvider;
import fr.etwin.utils.KubeChunk;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Date;

public class SocketChunkProvider implements KubeChunkProvider {
    private static final String serverHost = "127.0.0.1";
    private static final int serverPort = 2336;

    private Socket socket = null;
    private final long seed;

    public SocketChunkProvider(long seed) {
        this.seed = seed;
    }

    public boolean connect() {
        try {
            if (socket != null && socket.isConnected()) {
                return true;
            }
            socket = new Socket(serverHost, serverPort);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            socket = null;
        }
        return socket != null;
    }

    @Override
    public KubeChunk getChunk(int mX, int mY) {
        if (!this.connect()) {
            return null;
        }
        try {
            DataInputStream din = new DataInputStream(socket.getInputStream());
            DataOutputStream dout = new DataOutputStream(socket.getOutputStream());

            dout.writeBytes("%s:%s:%s!".formatted(seed, mX, mY));
            byte[] chunk = din.readNBytes(256 * 256 * 32);
            return new KubeChunk(mX, mY, chunk, false, new java.sql.Date(new Date().getTime()));
        } catch (IOException e) {
            System.err.println(e.getMessage());
            socket = null;
        }
        return null;
    }
}
